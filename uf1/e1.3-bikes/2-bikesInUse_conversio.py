#!/usr/bin/python3
# coding=utf-8

########################################################################
## AUTOR: 		Josep Maria Viñolas Auquer			      			  ##
## VERSIO: 		0.2													  ##
## LLICENCIA: 	AGPLv3                                                ##
## DESCRIPCIO:  Es conectarà a la api json de bicing, sumarà les bicis##
##              de cada estació i ho mostrarà per consola			  ## 
## REQUISITS:   sudo pip3 install requests					          ##
## EXECUCIO:	python3 2-bikesInUse_entrada.py                       ##
########################################################################

# Aquesta llibreria ens permet fer peticions a una URL.
# En el nostre cas l'usarem per conectar a les dades de la API.
import requests

# Ara a més importarem la llibreria time que té funcions com sleep per
# fer que esperi un cert temps el nostre programa
import time

# Definirem una variable iteració de tipus entera que tindrà com a valor
# inicial 0 i que s'anirà incrementant en el bucle while. Aquest bucle
# finalitzarà quan iteració sigui més gran que 10.

# Guardarem totes les dades que ens vagi retornant el web en una llista
# anomenada databicing. El sistema operatiu serà l'encarregat de trobar
# un lloc a la memòria RAM de l'ordinador on desar-les
databicing=[]
iteracio=0
while iteracio < 10:
	#######################
	#### ENTRADA DE DADES: Agafem les dades des de la URL de la API en 
	####  format JSON
    #######################
    dataapi=requests.get("https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status").json()
    time.sleep(1)
    iteracio=iteracio+1
    print("Petició Nº:"+str(iteracio))
    
    #######################
    #### CONVERSIO/PROCESSAT DE DADES: Sumarem el total de mecàniques i 
    ####  elèctriques de totes les estacions
    #######################
    totals={"mecaniques":0,
            "electriques":0}
    for station in dataapi["data"]["stations"]:
        totals["mecaniques"] = totals["mecaniques"] + station["num_bikes_available_types"]["mechanical"]
        totals["electriques"] = totals["electriques"] + station["num_bikes_available_types"]["ebike"]
    databicing.append(totals)

print(databicing)
