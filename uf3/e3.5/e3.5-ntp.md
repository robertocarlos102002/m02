# E5. Exercici 5. Sincronització del rellotge

## Introducció

Els sistemes operatius necessiten d'un rellotge en hora per fer funcionar molts dels seus serveis. Per posar un exemple, el sistema de *logs* implica tenir un rellotge fiable per tal de poder analitzar-los correctament.

Arreu del món existeixen rellotges (generalment amb base atòmica) que proporcionen senyals de molta precisió. Aquestes senyals són gestionades per servidors de rellotge que són accessibles i als que podem anar consultant per tal d'anar posant en hora constantment el rellotge del nostre ordinador.

Els ordinadors no tenen un rellotge atòmic (!!). El que tenen és un rellotge de quartz. Tot i que són molt fiables, aquests rellotges de quartz tenen menys precisió i això fa que al cap d'hores es vagin acumulant els errors i l'hora no sigui correcta.

Tenir el nostre rellotge en hora és important i més tenint en compte que hi ha serveis de xarxa que requereixen que el servidor i el client es trobin amb el rellotge sincronitzat.

## Continguts

Als sistemes Linux tradicionament s'ha fet servir el servei **ntpd** per a dur a terme aquesta sicronització. Actualment hi ha distribucions basades en Red Hat que han incorporat un nou servei per a fer-ho, el **chrony** que es mostra més fiable si no podem contactar constantment amb el servidor de temps.

- <https://docs.fedoraproject.org/en-US/fedora/rawhide/system-administrators-guide/servers/Configuring_NTP_Using_the_chrony_Suite/>
- <https://docs.fedoraproject.org/en-US/fedora/rawhide/system-administrators-guide/servers/Configuring_NTP_Using_ntpd/>

## Entrega

1. **Quins servidors de rellotge (ntp) de primer nivell (stratum 1) públics hi ha a Barcelona?**
- Servidor:
   - IPv4:
   - Font:
   - Ubicació:
2. **Comproveu al terminal quina és la data i hora actuals al vostre sistema operatiu.**
- Ordre i sortida:
3. **Comproveu quin servei està fent servir el vostre sistema operatiu per a mantenir-se sincronitzat en hora.**
   - Servei actiu:
   - Ordre i sortida:
4. **Quins servidors de ntp està fent servir per sincronitzar-se?**
   - Fitxer on estan desats (o comanda per a veure'ls):
   - Servidors:
5. **Busqueu al journald els logs corresponents al servei de ntpd i mostreu-ne els últims 10 aquí:**
   - Ordre i sortida: